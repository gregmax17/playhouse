/**
 * Handles the keyboard and mouse wheel events.
 * You can bind keys to actions and check if they are currently held down or pressed down after the last frame.
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * // setup up a bind on the space bar
 * Input.bind(Input.KEY.SPACE, "jump");
 * Input.bind(Input.KEY.Z, "charge");
 * 
 * // in your update/tick somewhere
 * if ( Input.pressed("jump") ) {
 * 	console.log("jump boi!");
 * }
 * 
 * let charge = 0;
 * if ( Input.state("charge") ) {
 * 	charge += Timer.delta;
 * }
 * if ( Input.released("charge") ) {
 * 	console.log("charged this much: " + charge);
 * 	charge = 0;
 * }
 * @namespace Input
 * @property {Object} bindings
 * @property {Object} actions
 * @property {Object} presses
 * @property {Object} locks
 * @property {Object} delayedKeyup
 * @property {Object} KEY
 * @property {Number} KEY.WHEEL_UP -4
 * @property {Number} KEY.WHEEL_DOWN -5
 * @property {Number} KEY.BACKSPACE 8
 * @property {Number} KEY.TAB 9
 * @property {Number} KEY.ENTER 13
 * @property {Number} KEY.PAUSE 19
 * @property {Number} KEY.CAPS 20
 * @property {Number} KEY.ESC 27
 * @property {Number} KEY.SPACE 32
 * @property {Number} KEY.PAGE_UP 33
 * @property {Number} KEY.PAGE_DOWN 34
 * @property {Number} KEY.END 35
 * @property {Number} KEY.HOME 36
 * @property {Number} KEY.LEFT_ARROW 37
 * @property {Number} KEY.UP_ARROW 38
 * @property {Number} KEY.RIGHT_ARROW 39
 * @property {Number} KEY.DOWN_ARROW 40
 * @property {Number} KEY.INSERT 45
 * @property {Number} KEY.DELETE 46
 * @property {Number} KEY._0 48
 * @property {Number} KEY._1 49
 * @property {Number} KEY._2 50
 * @property {Number} KEY._3 51
 * @property {Number} KEY._4 52
 * @property {Number} KEY._5 53
 * @property {Number} KEY._6 54
 * @property {Number} KEY._7 55
 * @property {Number} KEY._8 56
 * @property {Number} KEY._9 57
 * @property {Number} KEY.A 65
 * @property {Number} KEY.B 66
 * @property {Number} KEY.C 67
 * @property {Number} KEY.D 68
 * @property {Number} KEY.E 69
 * @property {Number} KEY.F 70
 * @property {Number} KEY.G 71
 * @property {Number} KEY.H 72
 * @property {Number} KEY.I 73
 * @property {Number} KEY.J 74
 * @property {Number} KEY.K 75
 * @property {Number} KEY.L 76
 * @property {Number} KEY.M 77
 * @property {Number} KEY.N 78
 * @property {Number} KEY.O 79
 * @property {Number} KEY.P 80
 * @property {Number} KEY.Q 81
 * @property {Number} KEY.R 82
 * @property {Number} KEY.S 83
 * @property {Number} KEY.T 84
 * @property {Number} KEY.U 85
 * @property {Number} KEY.V 86
 * @property {Number} KEY.W 87
 * @property {Number} KEY.X 88
 * @property {Number} KEY.Y 89
 * @property {Number} KEY.Z 90
 * @property {Number} KEY.NUMPAD_0 96
 * @property {Number} KEY.NUMPAD_1 97
 * @property {Number} KEY.NUMPAD_2 98
 * @property {Number} KEY.NUMPAD_3 99
 * @property {Number} KEY.NUMPAD_4 100
 * @property {Number} KEY.NUMPAD_5 101
 * @property {Number} KEY.NUMPAD_6 102
 * @property {Number} KEY.NUMPAD_7 103
 * @property {Number} KEY.NUMPAD_8 104
 * @property {Number} KEY.NUMPAD_9 105
 * @property {Number} KEY.MULTIPLY 106
 * @property {Number} KEY.ADD 107
 * @property {Number} KEY.SUBSTRACT 109
 * @property {Number} KEY.DECIMAL 110
 * @property {Number} KEY.DIVIDE 111
 * @property {Number} KEY.F1 112
 * @property {Number} KEY.F2 113
 * @property {Number} KEY.F3 114
 * @property {Number} KEY.F4 115
 * @property {Number} KEY.F5 116
 * @property {Number} KEY.F6 117
 * @property {Number} KEY.F7 118
 * @property {Number} KEY.F8 119
 * @property {Number} KEY.F9 120
 * @property {Number} KEY.F10 121
 * @property {Number} KEY.F11 122
 * @property {Number} KEY.F12 123
 * @property {Number} KEY.SHIFT 16
 * @property {Number} KEY.CTRL 17
 * @property {Number} KEY.ALT 18
 * @property {Number} KEY.PLUS 187
 * @property {Number} KEY.COMMA 188
 * @property {Number} KEY.MINUS 189
 * @property {Number} KEY.PERIOD 190
 */
class Input {
	/**
	 * Adds mouse wheel listener on the `document`, keybaord listener on the `window`, and a `blur` event on the `window`.
	 */
	constructor() {
		this.KEY = {
			WHEEL_UP: -4,
			WHEEL_DOWN: -5,
			BACKSPACE: 8,
			TAB: 9,
			ENTER: 13,
			PAUSE: 19,
			CAPS: 20,
			ESC: 27,
			SPACE: 32,
			PAGE_UP: 33,
			PAGE_DOWN: 34,
			END: 35,
			HOME: 36,
			LEFT_ARROW: 37,
			UP_ARROW: 38,
			RIGHT_ARROW: 39,
			DOWN_ARROW: 40,
			INSERT: 45,
			DELETE: 46,
			_0: 48,
			_1: 49,
			_2: 50,
			_3: 51,
			_4: 52,
			_5: 53,
			_6: 54,
			_7: 55,
			_8: 56,
			_9: 57,
			A: 65,
			B: 66,
			C: 67,
			D: 68,
			E: 69,
			F: 70,
			G: 71,
			H: 72,
			I: 73,
			J: 74,
			K: 75,
			L: 76,
			M: 77,
			N: 78,
			O: 79,
			P: 80,
			Q: 81,
			R: 82,
			S: 83,
			T: 84,
			U: 85,
			V: 86,
			W: 87,
			X: 88,
			Y: 89,
			Z: 90,
			NUMPAD_0: 96,
			NUMPAD_1: 97,
			NUMPAD_2: 98,
			NUMPAD_3: 99,
			NUMPAD_4: 100,
			NUMPAD_5: 101,
			NUMPAD_6: 102,
			NUMPAD_7: 103,
			NUMPAD_8: 104,
			NUMPAD_9: 105,
			MULTIPLY: 106,
			ADD: 107,
			SUBSTRACT: 109,
			DECIMAL: 110,
			DIVIDE: 111,
			F1: 112,
			F2: 113,
			F3: 114,
			F4: 115,
			F5: 116,
			F6: 117,
			F7: 118,
			F8: 119,
			F9: 120,
			F10: 121,
			F11: 122,
			F12: 123,
			SHIFT: 16,
			CTRL: 17,
			ALT: 18,
			PLUS: 187,
			COMMA: 188,
			MINUS: 189,
			PERIOD: 190
		};

		this.bindings = {};
		this.actions = {};
		this.presses = {};
		this.locks = {};
		this.delayedKeyup = {};

		document.addEventListener('mousewheel', (event) => this.mousewheel(event), false);
		document.addEventListener('DOMMouseScroll', (event) => this.mousewheel(event), false);

		window.addEventListener('keydown', (event) => this.keydown(event), false);
		window.addEventListener('keyup', (event) => this.keyup(event), false);

		window.addEventListener('blur', () => this.keysreset(), false);
	}

	/**
	 * Called internally.
	 * @param {WheelEvent} event {@link https://developer.mozilla.org/en-US/docs/Web/API/WheelEvent|WheelEvent}
	 */
	mousewheel(event) {
		let delta = event.wheelDelta ? event.wheelDelta : event.detail * -1;
		let code = delta > 0 ? this.KEY.WHEEL_UP : this.KEY.WHEEL_DOWN;
		let action = this.bindings[code];

		if (action) {
			this.actions[action] = true;
			this.presses[action] = true;
			this.delayedKeyup[action] = true;

			event.stopPropagation();
			event.preventDefault();
		}
	}

	/**
	 * Called internally.
	 * @param {KeyboardEvent} event {@link https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent|KeyboardEvent}
	 */
	keydown(event) {
		let tag = event.target.tagName.toLowerCase();
		if (tag === 'input' || tag === 'textarea' || tag === 'select') {
			return;
		}

		let action = this.bindings[event.keyCode];
		if (action) {
			this.actions[action] = true;

			if (!this.locks[action]) {
				this.presses[action] = true;
				this.locks[action] = true;
			}

			if (event.keyCode > 0) {
				event.stopPropagation();
				event.preventDefault();
			}
		}
	}

	/**
	 * Called internally.
	 * @param {KeyboardEvent} event {@link https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent|KeyboardEvent}
	 */
	keyup(event) {
		let tag = event.target.tagName.toLowerCase();
		if (tag === 'input' || tag === 'textarea' || tag === 'select') {
			return;
		}

		let action = this.bindings[event.keyCode];
		if (action) {
			this.delayedKeyup[action] = true;
			event.stopPropagation();
			event.preventDefault();
		}
	}

	/**
	 * Called internally. 
	 * Called on an {@link https://developer.mozilla.org/en-US/docs/Web/API/Window/blur|window.blur} event.
	 */
	keysreset() {
		for (let action in this.delayedKeyup) {
			this.actions[action] = false;
			delete this.delayedKeyup[action];
		}
	}

	/**
	 * Bind the keyboard key to an action. 
	 * Several buttons can be bound to the same action, but a button *can not* be bound to several actions.
	 * @example
	 * Input.bind(KEY.Z, "fire");
	 * Input.bind(32); // 32 is the space bar
	 * 
	 * // later in your code
	 * Input.released("fire"); 
	 * Input.released(32);
	 * @param {Number} key Can use any property value from {@link Input|Input.KEY}.
	 * @param {String} [action=false] Optional. If omitted you will need to check by the passed `key` value.
	 */
	bind(key, action = false) {
		this.bindings[key] = action || key + '';
	}

	/**
	 * Unbind a keyboard from its current action.
	 * @param {Number} key Can use any property value from {@link Input|Input.KEY}.
	 */
	unbind(key) {
		let action = this.bindings[key];
		this.delayedKeyup[action] = true;
		this.bindings[key] = null;
	}

	/**
	 * Unbind all buttons.
	 */
	unbindAll() {
		this.bindings = {};
		this.actions = {};
		this.presses = {};
		this.delayedKeyup = {};
	}

	/**
	 * Returns a `boolean` if one of the actions is held down.
	 * @param {Number|String} action
	 * @return {boolean}
	 */
	state(action) {
		return this.actions[action];
	}

	/**
	 * Returns a `boolean` if one of the actions was pressed.
	 * @param {Number|String} action 
	 * @return {boolean}
	 */
	pressed(action) {
		return this.presses[action];
	}

	/**
	 * Returns a `boolean` if one of the actions was just released.
	 * @param {Number|String} action 
	 * @return {boolean}
	 */
	released(action) {
		return !!this.delayedKeyup[action];
	}

	/**
	 * Called internally.
	 * Called from {@link Play|Play.update} each tick.
	 */
	clearPressed() {
		let action;

		for (action in this.delayedKeyup) {
			this.actions[action] = false;
			this.locks[action] = false;
			delete this.delayedKeyup[action];
		}

		for (action in this.presses) {
			delete this.presses[action];
		}
	}
}

export default new Input();