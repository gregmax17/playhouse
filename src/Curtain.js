import {default as Loader} from './Loader.js';
import {Play} from './Play.js';
import {Scene} from './Scene.js';

/**
 * A simple example of a Curtain class used for preloading and in game loading of assets.
 * This is not needed for your project, no need for extending this class, this is just a reference/example.
 */
class Curtain extends createjs.Container {
    /**
     * Setups the display for this Curtain.
     */
    setup() {
        let dimensions = Play.current.config.dimensions;
        let w2 = dimensions.width.max * 0.5;
        let h = dimensions.height.max;

        this.sideLeft = new createjs.Shape(new createjs.Graphics().f('#f00').dr(0, 0, w2, h)).set({ regX: w2 });
        this.sideRight = new createjs.Shape(new createjs.Graphics().f('#f00').dr(0, 0, w2, h));

        this.addChild(this.sideLeft, this.sideRight);

		// add this to the stage
		Play.current.stage.addChild(this);
    }

    /**
     * Transitions the display objects of this Curtain into view.
     * Calls `Scene.end` on the current Scene (`Scene.current.end`).
     * Calls `loadStart` after transition.
     * @param {Scene} nextScene 
     * @param {String|Array} sceneAssetTags
     */
    show(nextScene, sceneAssetTags) {
        // no children? setup it up!
        if (!this.children.length) {
            this.setup();
        }

        // end current scene
        if (Scene.current) {
            Scene.current.end();
        }

        this.isLoading = false;
        this.isComplete = false;

        this.mouseEnabled = false;
        this.visible = true;
        this.status = 0;

        this.sideLeft.x = 0;
        this.sideRight.x = Play.current.width;

        createjs.Tween.get(this.sideLeft).to({ x: Play.current.width * 0.5 }, 400, createjs.Ease.quadOut);
        createjs.Tween.get(this.sideRight).to({ x: Play.current.width * 0.5 }, 400, createjs.Ease.quadOut);

        // start the loading
        createjs.Tween.get(this).wait(800).call(this.loadStart, [nextScene, sceneAssetTags], this);
    }

    /**
     * Called via `loadComplete`, its time to hide this Curtain.
     * Calls `Scene.start` after transition out of view (`Scene.current.start`).
     */
    hide() {
        createjs.Tween.get(this.sideLeft).to({ x: 0 }, 400, createjs.Ease.quadIn);
        createjs.Tween.get(this.sideRight).to({ x: Play.current.width }, 400, createjs.Ease.quadIn);

        // let the scene know to start
        createjs.Tween.get(this).wait(800).set({ visible: false }).call(() => {
            if (Scene.current) {
                Scene.current.start();
            }
        });
    }

    /**
     * Starts loading the `nextScene`s asset tags by calling {@link Loader|Loader.load}.
     * @param {Scene} nextScene
     * @param {String|Array} sceneAssetTags 
     */
    loadStart(nextScene, sceneAssetTags) {
        this.isLoading = true;
        Loader.load(sceneAssetTags, () => {
            this.isLoading = false;
            this.loadComplete(nextScene);
        });
    }

    /**
     * Instantiates the `nextScene`.
     * Calls the `hide` method.
     * @param {Scene} nextScene 
     */
    loadComplete(nextScene) {
        // we are complete
        this.isComplete = true;

        // init the scene
        new nextScene();

        // hide it
        this.hide();
    }

    /**
     * Can be overwritten, can be used to show a progress bar.
     * Get the value by {@link Loader|Loader.status}.
     * @param {Object} event 
     */
    _tick(event) {
        // call the parent
        super._tick(event);
    }
}

export {Curtain};