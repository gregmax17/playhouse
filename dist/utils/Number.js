"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._Number = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @namespace _Number
 * @name Number
 */
var _Number =
/*#__PURE__*/
function () {
  function _Number() {
    _classCallCheck(this, _Number);
  }

  _createClass(_Number, null, [{
    key: "toOrdinal",

    /**
     * Will return a string based on the ordinal of the number.
     * @example
     * Number.toOrdinal(1); // '1st'
     * Number.toOrdinal(12); // '12th'
     * Number.toOrdinal(23); // '23rd'
     * @memberof Number
     * @name toOrdinal
     * @param {Number} number 
     * @return {String}
     */
    value: function toOrdinal(number) {
      var s = ['th', 'st', 'nd', 'rd'];
      var v = number % 100;
      return number + (s[(v - 20) % 20] || s[v] || s[0]);
    }
    /**
     * Maps the `number` value from a range of numbers. The return value is not clamped.
     * @example
     * Number.map(50, 0, 100, 0, 400); // 200
     * Number.map(0.5, 0, 1, 400, 800); // 600
     * Number.map(100, 0, 50, -100, 100); // 300
     * @memberof Number
     * @name map
     * @param {Number} number 
     * @param {Number} startMin 
     * @param {Number} startMax 
     * @param {Number} endMin 
     * @param {Number} endMax 
     * @return {Number}
     */

  }, {
    key: "map",
    value: function map(number, startMin, startMax, endMin, endMax) {
      return endMin + (endMax - endMin) * ((number - startMin) / (startMax - startMin));
    }
    /**
     * Returns a `String` commas format based on the `number`.
     * @example
     * Number.comma(1234567); // returns '1,234,567'
     * @memberof Number
     * @name comma
     * @param {Number} number 
     * @return {String}
     */

  }, {
    key: "comma",
    value: function comma(number) {
      return (number + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    /**
     * Returns the rounded `number` to the `precision` decimal places.
     * @example
     * Number.round(Math.PI); // returns 3
     * Number.round(Math.PI, 2); // returns 3.14
     * @memberof Number
     * @name round
     * @param {Number} number 
     * @param {Number} precision 
     * @return {Number}
     */

  }, {
    key: "round",
    value: function round(number, precision) {
      precision = Math.pow(10, precision || 0);
      return Math.round(number * precision) / precision;
    }
  }]);

  return _Number;
}();

exports._Number = _Number;