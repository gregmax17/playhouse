/**
 * Plays a sound loops as your backgroud music, it uses a {@link https://createjs.com/docs/soundjs/classes/Sound.html|CreateJS Sound} instance. 
 * You can transition from one background music to another, and it will auto fade in out (default milliseconds is `600`).
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * // load the assets tagged with "game"
 * Loader.load(["game"], () => {
 * 	// play the audio loaded that had the id "music", fading the volume to 80% over 1.2 seconds
 * 	Music.play("music", 0.8, 1200);
 * });
 * @namespace Music
 * @property {String} id Current `id` of the song being played.
 * @property {Sound} soundObj Calls `createjs.Sound.play`, see {@link https://createjs.com/docs/soundjs/classes/Sound.html|Sound}.
 */
class Music {
	id = '';
	soundObj = null;

	/**
	 * Plays and loops the sound. 
	 * @param {String} id The `id` should reference the same from your assets assigned via {@link Loader|Loader.setAssets}.
	 * @param {Number} [volume=1] The volume which the music will fade to when coming in.
	 * @param {Number} [fadeTime=600] The duration of the fading music (going out and coming in).
	 */
	play(id, volume, fadeTime) {
		if (this.id === id) {
			return;
		}

		this.id = id;

		if (this.soundObj) {
			this.fadeTo(0, fadeTime).wait(100).call(this._play, [id, volume, fadeTime], this);
		}
		else {
			this._play(id, volume, fadeTime);
		}
	}

	_play(id, volume = 1, fadeTime) {
		this.soundObj = createjs.Sound.play(id, { loop: Infinity, volume: 0 });
		this.fadeTo(volume, fadeTime);
	}

	/**
	 * Resumes the music.
	 * @return {Music}
	 */
	resume() {
		if (this.soundObj) {
			this.soundObj.paused = false;//setPaused(false);
		}
		return this;
	}

	/**
	 * Pauses the music.
	 * @return {Music}
	 */
	pause() {
		if (this.soundObj) {
			this.soundObj.paused = true;//setPaused(true);
		}
		return this;
	}

	/**
	 * @return {boolean}
	 */
	isPaused() {
		return this.soundObj ? this.soundObj.paused : false;
	}

	/**
	 * Stops the music and sets the `soundObj` instance to `null`.
	 * @param {Number} [fadeTime=0]
	 * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
	 */
	stop(fadeTime = 0) {
		return this.fadeTo(0, fadeTime).call(this.soundObj.stop, [], this.soundObj).call(() => {
			this.id = null;
			this.soundObj = null;
		});
	}

	/**
	 * Fades the music to the desired `volume` in milliseconds.
	 * @param {Number} volume 
	 * @param {Number} [fadeTime=600]
	 * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
	 */
	fadeTo(volume, fadeTime = 600) {
		if (this.soundObj) {
			return createjs.Tween.get(this.soundObj, { override: true }).to({ volume: volume }, fadeTime);
		}
		else {
			return createjs.Tween.get(this).wait(fadeTime);
		}
	}
}

export default new Music();