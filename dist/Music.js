"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Plays a sound loops as your backgroud music, it uses a {@link https://createjs.com/docs/soundjs/classes/Sound.html|CreateJS Sound} instance. 
 * You can transition from one background music to another, and it will auto fade in out (default milliseconds is `600`).
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * // load the assets tagged with "game"
 * Loader.load(["game"], () => {
 * 	// play the audio loaded that had the id "music", fading the volume to 80% over 1.2 seconds
 * 	Music.play("music", 0.8, 1200);
 * });
 * @namespace Music
 * @property {String} id Current `id` of the song being played.
 * @property {Sound} soundObj Calls `createjs.Sound.play`, see {@link https://createjs.com/docs/soundjs/classes/Sound.html|Sound}.
 */
var Music =
/*#__PURE__*/
function () {
  function Music() {
    _classCallCheck(this, Music);

    _defineProperty(this, "id", '');

    _defineProperty(this, "soundObj", null);
  }

  _createClass(Music, [{
    key: "play",

    /**
     * Plays and loops the sound. 
     * @param {String} id The `id` should reference the same from your assets assigned via {@link Loader|Loader.setAssets}.
     * @param {Number} [volume=1] The volume which the music will fade to when coming in.
     * @param {Number} [fadeTime=600] The duration of the fading music (going out and coming in).
     */
    value: function play(id, volume, fadeTime) {
      if (this.id === id) {
        return;
      }

      this.id = id;

      if (this.soundObj) {
        this.fadeTo(0, fadeTime).wait(100).call(this._play, [id, volume, fadeTime], this);
      } else {
        this._play(id, volume, fadeTime);
      }
    }
  }, {
    key: "_play",
    value: function _play(id) {
      var volume = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      var fadeTime = arguments.length > 2 ? arguments[2] : undefined;
      this.soundObj = createjs.Sound.play(id, {
        loop: Infinity,
        volume: 0
      });
      this.fadeTo(volume, fadeTime);
    }
    /**
     * Resumes the music.
     * @return {Music}
     */

  }, {
    key: "resume",
    value: function resume() {
      if (this.soundObj) {
        this.soundObj.paused = false; //setPaused(false);
      }

      return this;
    }
    /**
     * Pauses the music.
     * @return {Music}
     */

  }, {
    key: "pause",
    value: function pause() {
      if (this.soundObj) {
        this.soundObj.paused = true; //setPaused(true);
      }

      return this;
    }
    /**
     * @return {boolean}
     */

  }, {
    key: "isPaused",
    value: function isPaused() {
      return this.soundObj ? this.soundObj.paused : false;
    }
    /**
     * Stops the music and sets the `soundObj` instance to `null`.
     * @param {Number} [fadeTime=0]
     * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
     */

  }, {
    key: "stop",
    value: function stop() {
      var _this = this;

      var fadeTime = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      return this.fadeTo(0, fadeTime).call(this.soundObj.stop, [], this.soundObj).call(function () {
        _this.id = null;
        _this.soundObj = null;
      });
    }
    /**
     * Fades the music to the desired `volume` in milliseconds.
     * @param {Number} volume 
     * @param {Number} [fadeTime=600]
     * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
     */

  }, {
    key: "fadeTo",
    value: function fadeTo(volume) {
      var fadeTime = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 600;

      if (this.soundObj) {
        return createjs.Tween.get(this.soundObj, {
          override: true
        }).to({
          volume: volume
        }, fadeTime);
      } else {
        return createjs.Tween.get(this).wait(fadeTime);
      }
    }
  }]);

  return Music;
}();

var _default = new Music();

exports["default"] = _default;