"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Curtain = void 0;

var _Loader = _interopRequireDefault(require("./Loader.js"));

var _Play = require("./Play.js");

var _Scene = require("./Scene.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

/**
 * A simple example of a Curtain class used for preloading and in game loading of assets.
 * This is not needed for your project, no need for extending this class, this is just a reference/example.
 */
var Curtain =
/*#__PURE__*/
function (_createjs$Container) {
  _inherits(Curtain, _createjs$Container);

  function Curtain() {
    _classCallCheck(this, Curtain);

    return _possibleConstructorReturn(this, _getPrototypeOf(Curtain).apply(this, arguments));
  }

  _createClass(Curtain, [{
    key: "setup",

    /**
     * Setups the display for this Curtain.
     */
    value: function setup() {
      var dimensions = _Play.Play.current.config.dimensions;
      var w2 = dimensions.width.max * 0.5;
      var h = dimensions.height.max;
      this.sideLeft = new createjs.Shape(new createjs.Graphics().f('#f00').dr(0, 0, w2, h)).set({
        regX: w2
      });
      this.sideRight = new createjs.Shape(new createjs.Graphics().f('#f00').dr(0, 0, w2, h));
      this.addChild(this.sideLeft, this.sideRight); // add this to the stage

      _Play.Play.current.stage.addChild(this);
    }
    /**
     * Transitions the display objects of this Curtain into view.
     * Calls `Scene.end` on the current Scene (`Scene.current.end`).
     * Calls `loadStart` after transition.
     * @param {Scene} nextScene 
     * @param {String|Array} sceneAssetTags
     */

  }, {
    key: "show",
    value: function show(nextScene, sceneAssetTags) {
      // no children? setup it up!
      if (!this.children.length) {
        this.setup();
      } // end current scene


      if (_Scene.Scene.current) {
        _Scene.Scene.current.end();
      }

      this.isLoading = false;
      this.isComplete = false;
      this.mouseEnabled = false;
      this.visible = true;
      this.status = 0;
      this.sideLeft.x = 0;
      this.sideRight.x = _Play.Play.current.width;
      createjs.Tween.get(this.sideLeft).to({
        x: _Play.Play.current.width * 0.5
      }, 400, createjs.Ease.quadOut);
      createjs.Tween.get(this.sideRight).to({
        x: _Play.Play.current.width * 0.5
      }, 400, createjs.Ease.quadOut); // start the loading

      createjs.Tween.get(this).wait(800).call(this.loadStart, [nextScene, sceneAssetTags], this);
    }
    /**
     * Called via `loadComplete`, its time to hide this Curtain.
     * Calls `Scene.start` after transition out of view (`Scene.current.start`).
     */

  }, {
    key: "hide",
    value: function hide() {
      createjs.Tween.get(this.sideLeft).to({
        x: 0
      }, 400, createjs.Ease.quadIn);
      createjs.Tween.get(this.sideRight).to({
        x: _Play.Play.current.width
      }, 400, createjs.Ease.quadIn); // let the scene know to start

      createjs.Tween.get(this).wait(800).set({
        visible: false
      }).call(function () {
        if (_Scene.Scene.current) {
          _Scene.Scene.current.start();
        }
      });
    }
    /**
     * Starts loading the `nextScene`s asset tags by calling {@link Loader|Loader.load}.
     * @param {Scene} nextScene
     * @param {String|Array} sceneAssetTags 
     */

  }, {
    key: "loadStart",
    value: function loadStart(nextScene, sceneAssetTags) {
      var _this = this;

      this.isLoading = true;

      _Loader["default"].load(sceneAssetTags, function () {
        _this.isLoading = false;

        _this.loadComplete(nextScene);
      });
    }
    /**
     * Instantiates the `nextScene`.
     * Calls the `hide` method.
     * @param {Scene} nextScene 
     */

  }, {
    key: "loadComplete",
    value: function loadComplete(nextScene) {
      // we are complete
      this.isComplete = true; // init the scene

      new nextScene(); // hide it

      this.hide();
    }
    /**
     * Can be overwritten, can be used to show a progress bar.
     * Get the value by {@link Loader|Loader.status}.
     * @param {Object} event 
     */

  }, {
    key: "_tick",
    value: function _tick(event) {
      // call the parent
      _get(_getPrototypeOf(Curtain.prototype), "_tick", this).call(this, event);
    }
  }]);

  return Curtain;
}(createjs.Container);

exports.Curtain = Curtain;