"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * The Loader uses {@link https://createjs.com/docs/preloadjs/modules/PreloadJS.html|PreloadJS} to load the assets needed.
 * 
 * Before using it, _you must_ set an `Array` of assets, which each item in the array being an object.
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * // each object in the array MUST contain at least a "id", "src", and "tags" property
 * let assets = {
 *	{ "id": "config", "src": "media/data/config.json", "tags": ["pre"] },
 * 	{ "id": "sprites-ui", "src": "media/images/ui.json", "type": "spritesheet", "tags": ["title", "game"] },
 * 	{ "id": "sprites-title", "src": "media/images/title.json", "type": "spritesheet", "tags": ["title"] },
 * 	{ "id": "music", "src": "media/audio/music.ogg", "tags": ["game"] }
 * };
 * 
 * // set the assets to the Loader
 * Loader.setAssets(assets);
 * 
 * // load the assets that are tagged 'game'
 * Loader.load("game", () => {
 * 	alert("loaded assets with the tags 'game'!");
 * });
 * 
 * // can load multiple tags assets
 * Loader.load(["title", "game"], () => {
 * 	alert("loaded!");
 * });
 * @namespace Loader
 * @property {Number} status The current load status, useful for loaders.
 * @property {boolean} [useXHR=false] Whether to use XHR requests or not.
 * @property {String} basePath The base path to use when loading assets.
 * @property {Number} loadTimeout The time it takes to an item before timing out.
 */
var Loader =
/*#__PURE__*/
function () {
  function Loader() {
    _classCallCheck(this, Loader);

    _defineProperty(this, "status", 0);

    _defineProperty(this, "useXHR", false);

    _defineProperty(this, "basePath", '');

    _defineProperty(this, "loadTimeout", 1000 * 60);

    _defineProperty(this, "loadedObjects", {});

    _defineProperty(this, "plugins", []);

    this.addPlugin(createjs.Sound);
  }
  /**
   * Will return the loaded asset via `id` if it exists, or will return `null`.
   * @example
   * Loader.get("sprites-ui"); // will return null
   * 
   * // load the assets with the tag 'game'
   * Loader.load("game", () => {
   * 	let ss = Loader.get("sprites-ui"); // will return a sprite sheet object
   * 
   * 	// then...
   * 	createjs.Sprite(ss, "play-sprite");
   * });
   * @param {String} id 
   * @return {*}
   */


  _createClass(Loader, [{
    key: "get",
    value: function get(id) {
      return this.loadedObjects[id] || null;
    }
    /**
     * Will set a `value` via `id`, used mainly for internal use.
     * You may also set delete a reference to a loaded asset by setting its value to `null`.
     * @example
     * Loader.set("config", null); // gone, will have to load it again
     * @param {String} id 
     * @param {*} value 
     */

  }, {
    key: "set",
    value: function set(id, value) {
      this.loadedObjects[id] = value;

      if (value === null) {
        delete this.loadedObjects[id];
      }
    }
    /**
     * Add an external plugin which can be used.
     * @param {Class} plugin 
     */

  }, {
    key: "addPlugin",
    value: function addPlugin(plugin) {
      this.plugins.push(plugin);
    }
    /**
     * Used to load the assets by `tags`.
     * @example
     * Loader.load("title", (number, str) => {
     * 	console.log(number, "and", str);
     * }, [123, "some string value"]);
     * @param {String|Array} tags Can be either a String or an Array of strings.
     * @param {Function} [callback] Will be called once all assets are loaded by the tag name.
     * @param {Array} [params] The paramets to into the `callback` once loaded.
     * @param {Object} [scope] The scope to use on the `callback`.
     */

  }, {
    key: "load",
    value: function load(tags, callback, params, scope) {
      this.loadCallback = callback || function () {};

      this.loadParams = params || null;
      this.loadScope = scope || null;
      var manifest = this.getAssetsByTags(tags);

      for (var i = 0; i < manifest.length; i++) {
        if (this.get(manifest[i].id)) {
          manifest.splice(i, 1);
          i--;
          continue;
        }
      }

      if (manifest.length) {
        this.loadManifest(manifest);
      } else {
        this.handleComplete();
      }
    }
    /**
     * Called internally. 
     * Creates an intance of {@link https://createjs.com/docs/preloadjs/classes/LoadQueue.html|PreloadJS's LoadQueue} class.
     * @param {Array} manifest 
     */

  }, {
    key: "loadManifest",
    value: function loadManifest(manifest) {
      var _this = this;

      if (!this.loadQueue) {
        this.status = 0;
        this.loadQueue = new createjs.LoadQueue(this.useXHR, this.basePath);
        this.loadQueue.on('fileload', this.handleFileLoad, this); // let the browser max it self out, we want to load all we can to be quicker

        this.loadQueue.setMaxConnections(32); // apply the plugins

        this.plugins.forEach(function (plugin) {
          _this.loadQueue.installPlugin(plugin);
        }); // one minute timeout for loading

        createjs.LoadItem.LOAD_TIMEOUT_DEFAULT = this.loadTimeout;
      }

      this.loadQueue.loadManifest(manifest);
    }
    /**
     * Called internally. 
     * Sets the `status` property based on `loaded objects / objects needed to be load`.
     * @param {Object} event 
     */

  }, {
    key: "handleFileLoad",
    value: function handleFileLoad(event) {
      // update status
      this.status = this.loadQueue ? this.loadQueue.getItems(true).length / this.loadQueue.getItems().length : this.status;
      this.set(event.item.id, event.result);

      if (this.status >= 1) {
        this.handleComplete();
      }
    }
    /**
     * Called internally.
     * Will be called when all the assets have been loaded.
     * Destroys the `CreateJS.LoadQueue` instance.
     */

  }, {
    key: "handleComplete",
    value: function handleComplete() {
      // complete
      this.status = 1;
      var loadCallback = this.loadCallback;
      var loadScope = this.loadScope;
      var loadParams = this.loadParams;
      this.loadCallback = null;
      this.loadScope = null;
      this.loadParams = null;
      var loadQueue = this.loadQueue;
      this.loadQueue = null;

      if (loadQueue) {
        loadQueue.unregisterLoader();
        loadQueue.removeAll();
        loadQueue.removeAllEventListeners();
      }

      if (loadCallback) {
        loadCallback.apply(loadScope, loadParams);
      }
    }
    /**
     * The `Array` of assets to set for the `Loader`.
     * @param {Array} assets 
     */

  }, {
    key: "setAssets",
    value: function setAssets(assets) {
      this.assets = assets;
    }
    /**
     * Returns the asset object defined in the assets array (which should have been via {@link Loader|setAssets})
     * @param {String} id
     * @return {Object}
     */

  }, {
    key: "getAssetById",
    value: function getAssetById(id) {
      for (var i = 0; i < this.assets.length; i++) {
        if (this.assets[i].id === id) {
          return this.assets[i];
        }
      }

      return null;
    }
    /**
     * Will return an `Array` of the assets that match the `tags`
     * @param {String|Array} tags Can be either a String or an Array of strings.
     * @return {Array}
     */

  }, {
    key: "getAssetsByTags",
    value: function getAssetsByTags(tags) {
      if (!this.assets) {
        throw 'You need to let the `Loader` know about your assets! Call `Loader.setAssets([assets])` and pass the assets array!';
      }

      tags = typeof tags === 'string' ? [tags] : tags;
      var manifest = [];

      for (var i = 0; i < this.assets.length; i++) {
        for (var j = 0; j < tags.length; j++) {
          if ((this.assets[i].tags || []).indexOf(tags[j]) > -1) {
            manifest.push(this.assets[i]);
          }
        }
      }

      return manifest;
    }
  }]);

  return Loader;
}();

var _default = new Loader();

exports["default"] = _default;