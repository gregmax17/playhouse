"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * The Timer class allows you get the difference between the current time and timers target time.
 * 
 * It also allows you scale the overall time which is passing, and the time since the last tick (aka delta time).
 * @example
 * let t1 = new Timer(); // no target time
 * let t2 = new Timer(5); // 5 second target
 * 
 * // 3 seconds later
 * t1.delta(); // will return 3
 * t2.delta(); // will return -2
 * 
 * // set a 10 second target and pause it
 * t1.set(10).pause(); // and later you can call .resume()
 * 
 * // getting the time since last tick
 * Timer.delta; // will return something like 0.0167...
 * Timer.scale = 0.25; // scale the overall game (and EaselJS animations)
 * Timer.deltaScaled; // will return 0.004175...
 * Timer.time; // the overall time since the first tick of the play/game started
 */
var Timer =
/*#__PURE__*/
function () {
  /**
   * Can set the target time.
   * @param {Number} [seconds=0]
   */
  function Timer(seconds) {
    _classCallCheck(this, Timer);

    _defineProperty(this, "base", 0);

    _defineProperty(this, "target", 0);

    _defineProperty(this, "pausedAt", 0);

    this.base = Date.now();
    this.set(seconds);
  }
  /**
   * Set the target time.
   * @param {Number} [seconds=0]
   * @return {Timer}
   */


  _createClass(Timer, [{
    key: "set",
    value: function set(seconds) {
      this.target = seconds || 0;
      return this.reset();
    }
    /**
     * Resets the starting timer.
     * @return {Timer}
     */

  }, {
    key: "reset",
    value: function reset() {
      this.base = Timer.time;
      this.pausedAt = 0;
      return this;
    }
    /**
     * Returns the time in seconds relative to the timers target time.
     * @return {Number}
     */

  }, {
    key: "delta",
    value: function delta() {
      return (this.pausedAt || Timer.time) - this.base - this.target;
    }
    /**
     * Pauses the timer.
     * @return {Timer}
     */

  }, {
    key: "pause",
    value: function pause() {
      if (!this.pausedAt) {
        this.pausedAt = Timer.time;
      }

      return this;
    }
    /**
     * Resumes the timer.
     * @return {Timer}
     */

  }, {
    key: "resume",
    value: function resume() {
      if (this.pausedAt) {
        this.base += Timer.time - this.pausedAt;
        this.pausedAt = 0;
      }

      return this;
    }
  }]);

  return Timer;
}();
/**
 * @property {Number} time The time elapsed at the beginning of this frame. This is the time in seconds since the start of the game.
 * @readonly
 */


Timer.time = 0;
/**
 * @property {Number} scale The scale at which time is passing.
 */

Timer.scale = 1;
/**
 * @property {Number} delta The time in seconds it took to complete the last frame.
 * @readonly
 */

Timer.delta = 0;
/**
 * @property {Number} delta The time in seconds it took to complete the last frame _scaled_ based off of `Timer.scale`.
 * @readonly
 */

Timer.deltaScaled = 0;
/**
 * Called internally. Sets the {@link Timer|Timer.delta}, {@link Timer|Timer.time}, and {@link Timer|Timer.deltaScaled} values.
 * @param {Event} event {@link https://createjs.com/docs/easeljs/classes/Event.html|Event}
 */

Timer.step = function (event) {
  var deltaTime = event.delta * 0.001; // event.delta *= Timer.scale;

  Timer.delta = deltaTime;
  Timer.time += deltaTime;
  Timer.deltaScaled = deltaTime * Timer.scale;
};

var _default = Timer;
exports["default"] = _default;