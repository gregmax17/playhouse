"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._Array = void 0;

var _Math2 = require("./Math.js");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @namespace _Array
 * @name Array
 */
var _Array =
/*#__PURE__*/
function () {
  function _Array() {
    _classCallCheck(this, _Array);
  }

  _createClass(_Array, null, [{
    key: "random",

    /**
    * Returns a random element from the Array's index.
    * @example
    * Array.random(['B', 'I', 'N', 'G', 'O']); // will randomly return 'B', 'I', 'N', 'G', or 'O'
    * @memberof Array
    * @name random
    * @param {Array} array
    * @return {*}
    */
    value: function random(array) {
      return array.length === 1 ? array[0] : array[_Math2._Math.random(0, array.length - 1)];
    }
    /**
    * Removes the item from the Array, if it exists.
    * @example
    * let numbers = [0, 2, 4, 6, 8];
    * Number.erase(numbers, 4); // will return [0, 2, 6, 8]
    * @memberof Array
    * @name erase
    * @param {Array} array 
    * @param {*} item 
    * @return {Array}
    */

  }, {
    key: "erase",
    value: function erase(array, item) {
      var index = array.indexOf(item);

      if (index > -1) {
        array.splice(index, 1);
      }

      return array;
    }
    /**
     * Shuffles the passed Array.
     * @example
     * let numbers = [1, 2, 3, 4, 5];
     * Number.shuffle(numbers); // returns [2, 5, 1, 3, 4]
     * @memberof Array
     * @name shuffle
     * @param {Array} array 
     * @return {Array}
     */

  }, {
    key: "shuffle",
    value: function shuffle(array) {
      for (var j, x, i = array.length; i; j = parseInt(_Math2._Math.random() * i), x = array[--i], array[i] = array[j], array[j] = x) {}

      return array;
    }
  }]);

  return _Array;
}();

exports._Array = _Array;