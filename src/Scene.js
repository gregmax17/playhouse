import {Play} from './Play.js';

/**
 * The Scene class extends {@link https://createjs.com/docs/easeljs/classes/Container.html|Easel's Container} class.
 * Once a new Scene has been instantiated it will be added to the {@link Play|Play.stage}, and the current Scene will be removed along will any listeners attached to it.
 * 
 * To get the current Scene use `Scene.current`.
 * @property {Number} screenX Percent value (0-1) to keep the scene aligned horizontally based on `Play.width`. Set this value to `null` to bypass.
 * @property {Number} screenY Same as `screenX` but vertically using `Play.height`. Set this value to `null` to bypass.
 */
class Scene extends createjs.Container {
	screenX = 0.5;
	screenY = 0.5;

    constructor() {
        super();

        // clear out previous scene
        if (Scene.current) {
            Scene.current.parent.removeChild(Scene.current);
            Scene.current.removeAllEventListeners();
        }

        // this is the new scene, add it to the stage
		Scene.current = Play.current.stage.addChild(this);
    }
    
    /**
     * This will be called as soon as the {@link Curtain} has been removed (this is optional).
     */
    start() {

    }

    /**
     * This will be called as soon as the {@link Curtain} begins to show (this is optional).
     */
    end() {

    }

    _tick(event) {
        if (this.screenX) {
            this.x = Play.current.width * this.screenX;
        }
        if (this.screenY) {
            this.y = Play.current.height * this.screenY;
		}

        super._tick(event);
    }
}

/**
 * @property {Scene} current Our current Scene, if any.
 */
Scene.current = null;

export {Scene};