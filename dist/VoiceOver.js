"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _Loader = _interopRequireDefault(require("./Loader.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Can be used to play a sequence of audio files for in game tutorials.
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * // play one audio clip
 * VoiceOver.play("tutorial-step-1", () => {
 * 	console.log("tutorial step 1 voice is over");
 * });
 * 
 * // play a sequence of audio clips
 * VoiceOver.play(["step-1", "step-2"], () => {
 * 	console.log("tutorial steps are over");
 * });
 * 
 * let complicated = {
 * 	"id": "step-2",
 * 	"beginCallback": this.someOtherFtn,
 * 	"beginScope": this
 * };
 * VoiceOver.play(["step-1", complicated]);
 * @namespace VoiceOver
 * @property {boolean} isPlaying 
 * @property {Number} index
 * @property {Sound} soundObj Repeatly calls `createjs.Sound.play`, see {@link https://createjs.com/docs/soundjs/classes/Sound.html|Sound}.
 */
var VoiceOver =
/*#__PURE__*/
function (_createjs$EventDispat) {
  _inherits(VoiceOver, _createjs$EventDispat);

  function VoiceOver() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, VoiceOver);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(VoiceOver)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "isPlaying", false);

    _defineProperty(_assertThisInitialized(_this), "index", 0);

    _defineProperty(_assertThisInitialized(_this), "sequence", null);

    _defineProperty(_assertThisInitialized(_this), "completeCallback", null);

    _defineProperty(_assertThisInitialized(_this), "completeParams", null);

    _defineProperty(_assertThisInitialized(_this), "completeScope", null);

    _defineProperty(_assertThisInitialized(_this), "soundObj", null);

    _defineProperty(_assertThisInitialized(_this), "movieClips", []);

    return _this;
  }

  _createClass(VoiceOver, [{
    key: "play",
    // TODO

    /**
     * Plays an audio, or a sequence of audios. 
     * Can pass a `String` of the `id` audio clip to play, or an `Array` of `id`s to play, or an `Object` or an `Array` of `Object`s.
     * The `Object` can have several properties:
     * @example
     * let complex = [
     * 	"some-id",
     * 	{
     * 		"id": "next-id",
     * 		"beginCallback": () => { console.log("start of step 2 audio"); }
     * 	},
     * 	"third-audio",
     * 	{
     * 		"id": "dont-forget",
     * 		"delay": 1200, // wait 1.2 seconds
     * 		"endCallback": () => { console.log("will be called after this audio clip ends"); }
     * 	},
     * 	"final-audio-id"
     * ];
     * VoiceOver.play(complex, () => {
     * 	console.log("All that is complete!");
     * });
     * @param {Array|String|Object} sequence 
     * @param {String} sequence.id
     * @param {Number} sequence.delay The time in milliseconds to delay playing the audio clip.
     * @param {Function} sequence.beginCallback
     * @param {Array} sequence.beginParams
     * @param {Object} sequence.beginScope If omitted, `scope` will be referenced.
     * @param {Function} sequence.endCallback
     * @param {Array} sequence.endParams
     * @param {Object} sequence.endScope If omitted, `scope` will be referenced.
     * @param {Function} callback 
     * @param {Array} params 
     * @param {Object} scope
     * @return {VoiceOver}
     */
    value: function play(sequence, callback, params, scope) {
      this.stop();
      this.sequence = sequence instanceof Array ? sequence : [sequence];
      this.completeCallback = callback || null;
      this.completeScope = scope || null;
      this.completeParams = params || null;
      this.playNext();
      return this;
    }
  }, {
    key: "playNext",
    value: function playNext() {
      var sequence = this.sequence[this.index++];

      if (sequence) {
        createjs.Tween.get(this, {
          override: true
        }).wait(_typeof(sequence) === 'object' ? sequence.delay || 1 : 1).call(this.prepVO, [sequence], this);
      } else {
        this.stop(true);
      }
    }
  }, {
    key: "prepVO",
    value: function prepVO(sequence) {
      var _this2 = this;

      var id = _typeof(sequence) === 'object' ? sequence.id : sequence;
      var ev = new createjs.Event('voiceover-prep');
      ev.sequence = sequence;
      this.dispatchEvent(ev); // its already loaded

      if (_Loader["default"].get(id)) {
        this.startVO(sequence);
      } // need to load it now
      else {
          var asset = _Loader["default"].getAssetById(id);

          createjs.Sound.on('fileload', function () {
            _this2.startVO(sequence);
          }, this, true);
          createjs.Sound.removeSound(asset.src);
          createjs.Sound.registerSound(asset.src, id);
        }
    }
  }, {
    key: "startVO",
    value: function startVO(sequence) {
      var id = _typeof(sequence) === 'object' ? sequence.id : sequence;
      var ev = new createjs.Event('voiceover-start');
      ev.sequence = sequence;
      this.dispatchEvent(ev); // 1 millisecond delay so we can attach events, haha

      this.soundObj = createjs.Sound.play(id, {
        delay: 1
      });
      this.soundObj.on('succeeded', this.startedVO, this, true, sequence);
      this.soundObj.on('complete', this.endVO, this, true, sequence);
    }
  }, {
    key: "startedVO",
    value: function startedVO(event, data) {
      this.isPlaying = true;

      if (_typeof(data) === 'object' && typeof data.beginCallback === 'function') {
        data.beginCallback.apply(data.beginScope || this.completeScope, data.beginParams);
      }
    }
  }, {
    key: "endVO",
    value: function endVO(event, data) {
      var _this3 = this;

      // a small catch just in case we are fading out
      if (!this.isPlaying) {
        return;
      } // stop event


      var ev = new createjs.Event('voiceover-stop');
      this.dispatchEvent(ev); // only if its a function

      if (_typeof(data) === 'object' && typeof data.endCallback === 'function') {
        data.endCallback.apply(data.endScope || this.completeScope, data.endParams);
      } // remove listeners


      if (this.soundObj) {
        this.soundObj.removeAllEventListeners(); // remove sound if it was registerd

        var src = this.soundObj.src;

        if (createjs.Sound._preloadHash[src]) {
          // need to wait before removing it because its not 'finished' yet
          setTimeout(function () {
            createjs.Sound.removeSound(src);

            _this3.playNext();
          }, 0);
        } // onto the next
        else {
            this.playNext();
          }
      } else {
        // play the next one
        this.playNext();
      }
    }
    /**
     * Stops the current running sequence running.
     * If `callCompleteCallbacks` is set to `true` it will call the the complete callback, along with scope and params, when this sequence started.
     * @example
     * VoiceOver.play(["step-1", "step-2", "step-3"], () => {
     * 	console.log("now that the tut steps are done, set up the game!");
     * });
     * 
     * // sometime before the sequence finishes, like if the user wants to skip the tutorial
     * VoiceOver.stop(true);
     * @param {Boolean} [callCompleteCallbacks=false] 
     * @return {VoiceOver}
     */

  }, {
    key: "stop",
    value: function stop() {
      var callCompleteCallbacks = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      createjs.Tween.removeTweens(this);

      if (this.soundObj) {
        if (this.soundObj.src) {
          this.soundObj.stop(); // just another catch to remove the sound, prob not needed

          if (createjs.Sound._preloadHash[this.soundObj.src]) {
            createjs.Sound.removeSound(this.soundObj.src);
          }
        }

        this.soundObj.removeAllEventListeners();
        createjs.Tween.removeTweens(this.soundObj);
      }

      this.soundObj = null;
      this.index = 0;
      this.isPlaying = false;
      var completeCallback = this.completeCallback;
      var completeScope = this.completeScope;
      var completeParams = this.completeParams;
      this.completeCallback = null;
      this.completeScope = null;
      this.completeParams = null;

      if (callCompleteCallbacks && completeCallback) {
        completeCallback.apply(completeScope, completeParams);
      }

      return this;
    }
    /**
     * Fades the sequence out over time.
     * See {@link VoiceOver|VoiceOver.stop} for reference on `callCompleteCallbacks`.
     * @param {Number} [time=600] 
     * @param {Boolean} [callCompleteCallbacks=false]
     * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
     */

  }, {
    key: "fadeOut",
    value: function fadeOut() {
      var time = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 600;
      var callCompleteCallbacks = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      this.isPlaying = false;
      createjs.Tween.removeTweens(this);
      return createjs.Tween.get(this.soundObj || {}, {
        override: true
      }).to({
        volume: 0
      }, time).call(this.stop, [callCompleteCallbacks], this);
    }
    /**
     * Can be called manually, and is called internally when losing focus on the window.
     * Resumes the current running sound.
     * @return {VoiceOver}
     */

  }, {
    key: "resume",
    value: function resume() {
      if (this.soundObj) {
        this.soundObj.paused = false; //setPaused(false);
      }

      return this;
    }
    /**
     * Can be called manually, and is called internally when gaining focus on the window.
     * Pauses the current running sound.
     * @return {VoiceOver}
     */

  }, {
    key: "pause",
    value: function pause() {
      if (this.soundObj) {
        this.soundObj.paused = true; //setPaused(true);
      }

      return this;
    }
    /**
     * @return {boolean}
     */

  }, {
    key: "isPaused",
    value: function isPaused() {
      return this.soundObj ? this.soundObj.paused : false;
    }
  }]);

  return VoiceOver;
}(createjs.EventDispatcher);

var _default = new VoiceOver();

exports["default"] = _default;