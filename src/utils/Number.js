/**
 * @namespace _Number
 * @name Number
 */
class _Number {
	/**
	 * Will return a string based on the ordinal of the number.
	 * @example
	 * Number.toOrdinal(1); // '1st'
	 * Number.toOrdinal(12); // '12th'
	 * Number.toOrdinal(23); // '23rd'
	 * @memberof Number
	 * @name toOrdinal
	 * @param {Number} number 
	 * @return {String}
	 */
	static toOrdinal(number) {
		let s = ['th', 'st', 'nd', 'rd'];
		let v = number % 100;
		return number + (s[(v - 20) % 20] || s[v] || s[0]);
	}

	/**
	 * Maps the `number` value from a range of numbers. The return value is not clamped.
	 * @example
	 * Number.map(50, 0, 100, 0, 400); // 200
	 * Number.map(0.5, 0, 1, 400, 800); // 600
	 * Number.map(100, 0, 50, -100, 100); // 300
	 * @memberof Number
	 * @name map
	 * @param {Number} number 
	 * @param {Number} startMin 
	 * @param {Number} startMax 
	 * @param {Number} endMin 
	 * @param {Number} endMax 
	 * @return {Number}
	 */
	static map(number, startMin, startMax, endMin, endMax) {
		return endMin + (endMax - endMin) * ((number - startMin) / (startMax - startMin));
	}

	/**
	 * Returns a `String` commas format based on the `number`.
	 * @example
	 * Number.comma(1234567); // returns '1,234,567'
	 * @memberof Number
	 * @name comma
	 * @param {Number} number 
	 * @return {String}
	 */
	static comma(number) {
		return (number + '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	}

	/**
	 * Returns the rounded `number` to the `precision` decimal places.
	 * @example
	 * Number.round(Math.PI); // returns 3
	 * Number.round(Math.PI, 2); // returns 3.14
	 * @memberof Number
	 * @name round
	 * @param {Number} number 
	 * @param {Number} precision 
	 * @return {Number}
	 */
	static round(number, precision) {
		precision = Math.pow(10, precision || 0);
		return Math.round(number * precision) / precision;
	}
}

export { _Number };