"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUrlVar = getUrlVar;

/**
 * 
 * @param {String} name The name of the variable in the URL Query string (ie, .../index.html?var1=value&var_2=value_2)
 * @return {String|null}
 */
function getUrlVar(name) {
  var vars = {};
  window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
    vars[key] = value;
  });
  return vars[name] || null;
}