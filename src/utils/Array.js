import { _Math } from './Math.js';

/**
 * @namespace _Array
 * @name Array
 */
class _Array {
	/**
	* Returns a random element from the Array's index.
	* @example
	* Array.random(['B', 'I', 'N', 'G', 'O']); // will randomly return 'B', 'I', 'N', 'G', or 'O'
	* @memberof Array
	* @name random
	* @param {Array} array
	* @return {*}
	*/
	static random(array) {
		return array.length === 1 ? array[0] : array[_Math.random(0, array.length - 1)];
	}

	/**
	* Removes the item from the Array, if it exists.
	* @example
	* let numbers = [0, 2, 4, 6, 8];
	* Number.erase(numbers, 4); // will return [0, 2, 6, 8]
	* @memberof Array
	* @name erase
	* @param {Array} array 
	* @param {*} item 
	* @return {Array}
	*/
	static erase(array, item) {
		let index = array.indexOf(item);
		if (index > -1) {
			array.splice(index, 1);
		}
		return array;
	}

	/**
	 * Shuffles the passed Array.
	 * @example
	 * let numbers = [1, 2, 3, 4, 5];
	 * Number.shuffle(numbers); // returns [2, 5, 1, 3, 4]
	 * @memberof Array
	 * @name shuffle
	 * @param {Array} array 
	 * @return {Array}
	 */
	static shuffle(array) {
		for (let j, x, i = array.length; i; j = parseInt(_Math.random() * i), x = array[--i], array[i] = array[j], array[j] = x) { }
		return array;
	}
}

export { _Array };