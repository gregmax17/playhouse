/**
 * The Timer class allows you get the difference between the current time and timers target time.
 * 
 * It also allows you scale the overall time which is passing, and the time since the last tick (aka delta time).
 * @example
 * let t1 = new Timer(); // no target time
 * let t2 = new Timer(5); // 5 second target
 * 
 * // 3 seconds later
 * t1.delta(); // will return 3
 * t2.delta(); // will return -2
 * 
 * // set a 10 second target and pause it
 * t1.set(10).pause(); // and later you can call .resume()
 * 
 * // getting the time since last tick
 * Timer.delta; // will return something like 0.0167...
 * Timer.scale = 0.25; // scale the overall game (and EaselJS animations)
 * Timer.deltaScaled; // will return 0.004175...
 * Timer.time; // the overall time since the first tick of the play/game started
 */
class Timer {
	base = 0;
	target = 0;
	pausedAt = 0;
	
    /**
     * Can set the target time.
     * @param {Number} [seconds=0]
     */
    constructor(seconds) {
        this.base = Date.now();
        this.set(seconds);
    }

    /**
     * Set the target time.
     * @param {Number} [seconds=0]
     * @return {Timer}
     */
    set(seconds) {
        this.target = seconds || 0;
        return this.reset();
    }

    /**
     * Resets the starting timer.
     * @return {Timer}
     */
    reset() {
        this.base = Timer.time;
        this.pausedAt = 0;
        return this;
    }

    /**
     * Returns the time in seconds relative to the timers target time.
     * @return {Number}
     */
    delta() {
        return (this.pausedAt || Timer.time) - this.base - this.target;
    }

    /**
     * Pauses the timer.
     * @return {Timer}
     */
    pause() {
        if (!this.pausedAt) {
            this.pausedAt = Timer.time;
        }
        return this;
    }

    /**
     * Resumes the timer.
     * @return {Timer}
     */
    resume() {
        if (this.pausedAt) {
            this.base += Timer.time - this.pausedAt;
            this.pausedAt = 0;
        }
        return this;
    }
}

/**
 * @property {Number} time The time elapsed at the beginning of this frame. This is the time in seconds since the start of the game.
 * @readonly
 */
Timer.time = 0;

/**
 * @property {Number} scale The scale at which time is passing.
 */
Timer.scale = 1;

/**
 * @property {Number} delta The time in seconds it took to complete the last frame.
 * @readonly
 */
Timer.delta = 0;

/**
 * @property {Number} delta The time in seconds it took to complete the last frame _scaled_ based off of `Timer.scale`.
 * @readonly
 */
Timer.deltaScaled = 0;

/**
 * Called internally. Sets the {@link Timer|Timer.delta}, {@link Timer|Timer.time}, and {@link Timer|Timer.deltaScaled} values.
 * @param {Event} event {@link https://createjs.com/docs/easeljs/classes/Event.html|Event}
 */
Timer.step = (event) => {
    let deltaTime = event.delta * 0.001;
    // event.delta *= Timer.scale;
    Timer.delta = deltaTime;
    Timer.time += deltaTime;
    Timer.deltaScaled = deltaTime * Timer.scale;
};

export default Timer;