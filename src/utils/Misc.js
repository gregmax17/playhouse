/**
 * 
 * @param {String} name The name of the variable in the URL Query string (ie, .../index.html?var1=value&var_2=value_2)
 * @return {String|null}
 */
export function getUrlVar(name) {
	let vars = {};
	window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) { vars[key] = value; });
	return vars[name] || null;
}