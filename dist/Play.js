"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Play = void 0;

var _Device = _interopRequireDefault(require("./Device.js"));

var _Input = _interopRequireDefault(require("./Input.js"));

var _Timer = _interopRequireDefault(require("./Timer.js"));

var _Music = _interopRequireDefault(require("./Music.js"));

var _VoiceOver = _interopRequireDefault(require("./VoiceOver.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// let isFocused = false;
var resizeInterval = 0;
var musicPaused = false;
var voiceOverPaused = false;
var soundMuted = false;
var soundVolume = 1;
var lastWidth = 0;
var lastHeight = 0;
/**
 * Sets up the basic elements, events, and stage to use.
 * @example
 * class MyPlay extends Play {
 *      constructor() {
 *          super();
 *          console.log("Hello Everybody!");
 *      }
 * }
 * 
 * Loader.load("pre", () => {
 * 	window.myPlay = new MyPlay({
 * 		canvas: "the-canvas-id",
 * 		dimensions: {
 * 			width: { min: 1024, max: 1664 },
 * 			height: { min: 640, max: 768 }
 * 		}
 * 	});
 * });
 * @namespace Play
 * @property {Number} width The current width of the canvas.
 * @property {Number} height The current height of the canvas.
 * @property {Stage} stage An instance of {@link https://createjs.com/docs/easeljs/classes/Stage.html|CreateJS.Stage}, calls the `update` method every tick.
 * @property {Object} config A reference to the config object passed when instantiating the Play.
 * @property {HTMLElement|String} config.canvas The canavs element or id of the canvas element.
 * @property {Object} config.dimensions The dimensions of the canvas (See example above ^).
 * @property {String} config.wrapper The wrapper which scales and resizes the canvas, if not specified the `window.innerWidth` and `window.innerHeight` will be referenced.
 * @property {Boolean} [config.wrapperScale=true] Sets the wrapper style width and height to 100%, if `config.wrapper` was specified.
 * @property {Boolean} [config.autoStop=true] When losing focus on the window, it will mute the sounds, pause the Music, Voice Over, and destroy the Ticker. When gaining focus it will recreate the Ticker and resume all sounds.
 * @property {Boolean} [config.autoCenter=true] Applies `marginTop` and `marginLeft` styles to the canvas to keep it centered in the wrapper.
 * @property {Boolean} [config.preventDefault=true] Calls `event.preventDefault` on the `touchmove` event.
 * @property {Boolean} [config.autoFocus=true] Calls `window.focus`.
 * @property {Number} [config.maxDelta=50] So the tick event doesn't fall below the minimum frame rate (1000 / 20 = 50, 20fps minimum).
 * @property {HTMLElement|String} config.dom Scales this DOM Element with the canvas (used for Text, UI, etc).
 */

var Play =
/*#__PURE__*/
function () {
  function Play(config) {
    _classCallCheck(this, Play);

    _defineProperty(this, "config", null);

    _defineProperty(this, "stage", null);

    _defineProperty(this, "width", 1);

    _defineProperty(this, "height", 1);

    // assign this new instance
    Play.current = this; // lets set up default options

    this.config = Object.assign({
      autoStop: true,
      wrapperScale: true,
      autoCenter: true,
      preventDefault: true,
      autoFocus: true,
      maxDelta: 1000 / 20 // 20 fps minimum

    }, config); // fix audio on cordova

    if (_Device["default"].cordova) {
      _Device["default"].fixWebAudio();
    }

    this.setupCreateJS();
    this.setupElements();
    this.setupEvents();
    this.setupStage(); // fix size now before first tick

    this.updateSize();
    createjs.Ticker.on('tick', this.update, this);
  }
  /**
   * Called internally.
   * Updates the canvas window, calls {@link Timer|Timer.step}, calls {@link Play|Play.update}, calls `update` on the {@link https://createjs.com/docs/easeljs/classes/Stage.html|CreateJS.Stage} stage, and finally {@link Input|Input.clearPressed}.
   * @param {Object} event 
   */


  _createClass(Play, [{
    key: "update",
    value: function update(event) {
      resizeInterval += event.delta;

      if (resizeInterval >= 500) {
        resizeInterval = 0;
        this.updateSize();
      } // update the timer


      _Timer["default"].step(event); // update the stage, render it


      this.stage.update(event); // clear inputs

      _Input["default"].clearPressed();
    }
    /**
     * Called interally, called every 500 milliseconds. Returns `true` if size was updated, `false` otherwise.
     * @return {Boolean}
     */

  }, {
    key: "updateSize",
    value: function updateSize() {
      var config = this.config,
          dimensions = config.dimensions,
          canvas = this.stage.canvas,
          bounds = config.wrapper === document.body ? {
        width: window.innerWidth,
        height: window.innerHeight
      } : (config.wrapper || canvas).getBoundingClientRect(),
          scale = Math.min(bounds.width / dimensions.width.min, bounds.height / dimensions.height.min),
          newWidth = Math.min(bounds.width / scale, dimensions.width.max),
          newHeight = Math.min(bounds.height / scale, dimensions.height.max);

      if (lastWidth === bounds.width && lastHeight === bounds.height) {
        return false;
      }

      lastWidth = bounds.width;
      lastHeight = bounds.height; // update the width and height

      this.width = canvas.width = newWidth;
      this.height = canvas.height = newHeight; // scale the width and height of the css

      canvas.style.width = "".concat(newWidth * scale, "px");
      canvas.style.height = "".concat(newHeight * scale, "px"); // center the game with css margin

      if (config.autoCenter) {
        canvas.style.marginTop = "".concat((bounds.height - newHeight * scale) * 0.5, "px");
        canvas.style.marginLeft = "".concat((bounds.width - newWidth * scale) * 0.5, "px");
      } // now the dom, this should be centered via css
      // we are just changing the scale of it so it matches the game


      if (config.dom) {
        var domStyle = config.dom.style;
        config.dom.transform = domStyle['-moz-transform'] = domStyle['-webkit-transform'] = domStyle['-ms-transform'] = "scale(".concat(scale, ")");
      }

      return true;
    }
    /**
     * Called internally, from the cosntructor.
     */

  }, {
    key: "setupCreateJS",
    value: function setupCreateJS() {
      // set some props
      createjs.Ticker.timingMode = createjs.Ticker.RAF; // request animation frame

      createjs.Ticker.maxDelta = this.config.maxDelta; // aka min frame rate, 20 is low enough!
      // set up sound

      createjs.Sound.registerPlugins([createjs.WebAudioPlugin, createjs.HTMLAudioPlugin]);
      createjs.Sound.alternateExtensions = ['mp3', 'ogg'];
      createjs.Sound.EXTENSION_MAP.mp3 = 'mpeg';
    }
    /**
     * Called internally, from the constructor.
     * Gets reference to the wrapper and dom elements.
     */

  }, {
    key: "setupElements",
    value: function setupElements() {
      var config = this.config; // wrapper

      config.wrapper = typeof config.wrapper === 'string' ? document.getElementById(config.wrapper) : config.wrapper || document.body; // scale it?

      if (config.wrapperScale && config.wrapper !== document.body) {
        config.wrapper.style.width = '100%';
        config.wrapper.style.height = '100%';
      } // get the dom


      config.dom = typeof config.dom === 'string' ? document.getElementById(config.dom) : config.dom;
    }
    /**
     * Called internally, from the constructor.
     * Sets up the window's page show and hide events (focus, blur, visibility, etc).
     */

  }, {
    key: "setupEvents",
    value: function setupEvents() {
      var _this = this;

      var config = this.config;
      var hidden = null; // we don't like this

      document.addEventListener('touchmove', function (event) {
        if (config.preventDefault) {
          event.preventDefault();
        }
      });

      if (document.hidden !== undefined) {
        hidden = 'visibilitychange';
      } else {
        ['webkit', 'moz', 'ms'].forEach(function (prefix) {
          if (document["".concat(prefix, "Hidden")] !== undefined) {
            hidden = "".concat(prefix, "Hidden");
          }
        });
      }

      var onChange = function onChange(event) {
        if (document.hidden || event.type === 'pause') {
          if (config.autoStop) {
            _this.onPageHide();
          }
        } else {
          if (config.autoStop) {
            _this.onPageShow();
          }
        }
      };

      if (hidden) {
        document.addEventListener(hidden, onChange, false);
      }

      window.addEventListener('focus', function () {
        if (config.autoStop) {
          _this.onPageShow();
        }
      }, false);
      window.addEventListener('blur', function () {
        if (config.autoStop) {
          _this.onPageHide();
        }
      }, false); // auto focus on the window

      if (config.autoFocus) {
        window.focus();
      }
    }
    /**
     * Called internally, from the constructor.
     * Sets up the `stage` property.
     */

  }, {
    key: "setupStage",
    value: function setupStage() {
      this.stage = new createjs.Stage(this.config.canvas);
      this.stage.preventSelection = false;
      this.stage.snapToPixelEnabled = false; // enable touch events

      if (_Device["default"].mobile) {
        createjs.Touch.enable(this.stage);
        this.stage.enableDOMEvents(false);
      } else {
        this.stage.enableMouseOver(20);
      }
    }
    /**
     * Called internally.
     * Unmutes audio, resumes Music and Voice Over, recreates the Ticker.
     */

  }, {
    key: "onPageShow",
    value: function onPageShow() {
      // if (isFocused) {
      // 	return;
      // }
      // isFocused = true;
      if (createjs.Ticker._timerId) {
        return;
      } // re-create the ticker


      createjs.Ticker._setupTick(); // resume sound


      createjs.Sound.muted = soundMuted;
      createjs.Sound.volume = soundVolume; // resume the current playing sounds (no sfx, yet...)

      if (!musicPaused) {
        _Music["default"].resume();
      }

      if (!voiceOverPaused) {
        _VoiceOver["default"].resume();
      }
    }
    /**
     * Called internally.
     * Mutes audio, pauses Music and Voice Over, destroys the Ticker.
     */

  }, {
    key: "onPageHide",
    value: function onPageHide() {
      // if (!isFocused) {
      // 	return;
      // }
      // isFocused = false;
      if (!createjs.Ticker._timerId) {
        return;
      } // remove the ticker


      if (createjs.Ticker._raf) {
        var f = window.cancelAnimationFrame || window.webkitCancelAnimationFrame || window.mozCancelAnimationFrame || window.oCancelAnimationFrame || window.msCancelAnimationFrame;
        f && f(createjs.Ticker._timerId);
      } else {
        clearTimeout(createjs.Ticker._timerId);
      }

      createjs.Ticker._timerId = null; // sounds

      soundMuted = createjs.Sound.muted;
      soundVolume = createjs.Sound.volume;
      createjs.Sound.muted = true;
      createjs.Sound.volume = 0; // pause the current playing sounds (no sfx, yet...)

      musicPaused = _Music["default"].isPaused();
      voiceOverPaused = _VoiceOver["default"].isPaused();

      _Music["default"].pause();

      _VoiceOver["default"].pause();
    }
  }]);

  return Play;
}();
/**
 * @property {Play} current Our current Play, if any.
 */


exports.Play = Play;
Play.current = null;