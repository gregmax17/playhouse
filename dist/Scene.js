"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Scene = void 0;

var _Play = require("./Play.js");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * The Scene class extends {@link https://createjs.com/docs/easeljs/classes/Container.html|Easel's Container} class.
 * Once a new Scene has been instantiated it will be added to the {@link Play|Play.stage}, and the current Scene will be removed along will any listeners attached to it.
 * 
 * To get the current Scene use `Scene.current`.
 * @property {Number} screenX Percent value (0-1) to keep the scene aligned horizontally based on `Play.width`. Set this value to `null` to bypass.
 * @property {Number} screenY Same as `screenX` but vertically using `Play.height`. Set this value to `null` to bypass.
 */
var Scene =
/*#__PURE__*/
function (_createjs$Container) {
  _inherits(Scene, _createjs$Container);

  function Scene() {
    var _this;

    _classCallCheck(this, Scene);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Scene).call(this)); // clear out previous scene

    _defineProperty(_assertThisInitialized(_this), "screenX", 0.5);

    _defineProperty(_assertThisInitialized(_this), "screenY", 0.5);

    if (Scene.current) {
      Scene.current.parent.removeChild(Scene.current);
      Scene.current.removeAllEventListeners();
    } // this is the new scene, add it to the stage


    Scene.current = _Play.Play.current.stage.addChild(_assertThisInitialized(_this));
    return _this;
  }
  /**
   * This will be called as soon as the {@link Curtain} has been removed (this is optional).
   */


  _createClass(Scene, [{
    key: "start",
    value: function start() {}
    /**
     * This will be called as soon as the {@link Curtain} begins to show (this is optional).
     */

  }, {
    key: "end",
    value: function end() {}
  }, {
    key: "_tick",
    value: function _tick(event) {
      if (this.screenX) {
        this.x = _Play.Play.current.width * this.screenX;
      }

      if (this.screenY) {
        this.y = _Play.Play.current.height * this.screenY;
      }

      _get(_getPrototypeOf(Scene.prototype), "_tick", this).call(this, event);
    }
  }]);

  return Scene;
}(createjs.Container);
/**
 * @property {Scene} current Our current Scene, if any.
 */


exports.Scene = Scene;
Scene.current = null;