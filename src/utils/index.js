import { _Array } from './Array.js';
import { _Math } from './Math.js';
import { _Number } from './Number.js';

import { getUrlVar } from './Misc.js';

/**
 * @namespace Utils
 */
const Utils = {
	Array: _Array,
	Math: _Math,
	Number: _Number,

	getUrlVar: getUrlVar
};

export { Utils };