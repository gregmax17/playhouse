import Loader from './Loader.js';

/**
 * Can be used to play a sequence of audio files for in game tutorials.
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * // play one audio clip
 * VoiceOver.play("tutorial-step-1", () => {
 * 	console.log("tutorial step 1 voice is over");
 * });
 * 
 * // play a sequence of audio clips
 * VoiceOver.play(["step-1", "step-2"], () => {
 * 	console.log("tutorial steps are over");
 * });
 * 
 * let complicated = {
 * 	"id": "step-2",
 * 	"beginCallback": this.someOtherFtn,
 * 	"beginScope": this
 * };
 * VoiceOver.play(["step-1", complicated]);
 * @namespace VoiceOver
 * @property {boolean} isPlaying 
 * @property {Number} index
 * @property {Sound} soundObj Repeatly calls `createjs.Sound.play`, see {@link https://createjs.com/docs/soundjs/classes/Sound.html|Sound}.
 */
class VoiceOver extends createjs.EventDispatcher {
	isPlaying = false;
	index = 0;
	sequence = null;
	completeCallback = null;
	completeParams = null;
	completeScope = null;
	soundObj = null;
	movieClips = []; // TODO

	/**
	 * Plays an audio, or a sequence of audios. 
	 * Can pass a `String` of the `id` audio clip to play, or an `Array` of `id`s to play, or an `Object` or an `Array` of `Object`s.
	 * The `Object` can have several properties:
	 * @example
	 * let complex = [
	 * 	"some-id",
	 * 	{
	 * 		"id": "next-id",
	 * 		"beginCallback": () => { console.log("start of step 2 audio"); }
	 * 	},
	 * 	"third-audio",
	 * 	{
	 * 		"id": "dont-forget",
	 * 		"delay": 1200, // wait 1.2 seconds
	 * 		"endCallback": () => { console.log("will be called after this audio clip ends"); }
	 * 	},
	 * 	"final-audio-id"
	 * ];
	 * VoiceOver.play(complex, () => {
	 * 	console.log("All that is complete!");
	 * });
	 * @param {Array|String|Object} sequence 
	 * @param {String} sequence.id
	 * @param {Number} sequence.delay The time in milliseconds to delay playing the audio clip.
	 * @param {Function} sequence.beginCallback
	 * @param {Array} sequence.beginParams
	 * @param {Object} sequence.beginScope If omitted, `scope` will be referenced.
	 * @param {Function} sequence.endCallback
	 * @param {Array} sequence.endParams
	 * @param {Object} sequence.endScope If omitted, `scope` will be referenced.
	 * @param {Function} callback 
	 * @param {Array} params 
	 * @param {Object} scope
	 * @return {VoiceOver}
	 */
	play(sequence, callback, params, scope) {
		this.stop();

		this.sequence = sequence instanceof Array ? sequence : [sequence];
		this.completeCallback = callback || null;
		this.completeScope = scope || null;
		this.completeParams = params || null;

		this.playNext();

		return this;
	}

	playNext() {
		let sequence = this.sequence[this.index++];

		if (sequence) {
			createjs.Tween.get(this, { override: true }).wait(typeof sequence === 'object' ? sequence.delay || 1 : 1).call(this.prepVO, [sequence], this);
		}
		else {
			this.stop(true);
		}
	}

	prepVO(sequence) {
		let id = typeof sequence === 'object' ? sequence.id : sequence;

		let ev = new createjs.Event('voiceover-prep');
		ev.sequence = sequence;
		this.dispatchEvent(ev);

		// its already loaded
		if (Loader.get(id)) {
			this.startVO(sequence);
		}
		// need to load it now
		else {
			let asset = Loader.getAssetById(id);

			createjs.Sound.on('fileload', () => {
				this.startVO(sequence);
			}, this, true);

			createjs.Sound.removeSound(asset.src);
			createjs.Sound.registerSound(asset.src, id);
		}
	}

	startVO(sequence) {
		let id = typeof sequence === 'object' ? sequence.id : sequence;

		let ev = new createjs.Event('voiceover-start');
		ev.sequence = sequence;
		this.dispatchEvent(ev);

		// 1 millisecond delay so we can attach events, haha
		this.soundObj = createjs.Sound.play(id, { delay: 1 });
		this.soundObj.on('succeeded', this.startedVO, this, true, sequence);
		this.soundObj.on('complete', this.endVO, this, true, sequence);
	}

	startedVO(event, data) {
		this.isPlaying = true;

		if (typeof data === 'object' && typeof data.beginCallback === 'function') {
			data.beginCallback.apply(data.beginScope || this.completeScope, data.beginParams);
		}
	}

	endVO(event, data) {
		// a small catch just in case we are fading out
		if (!this.isPlaying) {
			return;
		}

		// stop event
		let ev = new createjs.Event('voiceover-stop');
		this.dispatchEvent(ev);

		// only if its a function
		if (typeof data === 'object' && typeof data.endCallback === 'function') {
			data.endCallback.apply(data.endScope || this.completeScope, data.endParams);
		}

		// remove listeners
		if (this.soundObj) {
			this.soundObj.removeAllEventListeners();

			// remove sound if it was registerd
			let src = this.soundObj.src;
			if (createjs.Sound._preloadHash[src]) {
				// need to wait before removing it because its not 'finished' yet
				setTimeout(() => {
					createjs.Sound.removeSound(src);
					this.playNext();
				}, 0);
			}
			// onto the next
			else {
				this.playNext();
			}
		}
		else {
			// play the next one
			this.playNext();
		}
	}

	/**
	 * Stops the current running sequence running.
	 * If `callCompleteCallbacks` is set to `true` it will call the the complete callback, along with scope and params, when this sequence started.
	 * @example
	 * VoiceOver.play(["step-1", "step-2", "step-3"], () => {
	 * 	console.log("now that the tut steps are done, set up the game!");
	 * });
	 * 
	 * // sometime before the sequence finishes, like if the user wants to skip the tutorial
	 * VoiceOver.stop(true);
	 * @param {Boolean} [callCompleteCallbacks=false] 
	 * @return {VoiceOver}
	 */
	stop(callCompleteCallbacks = false) {
		createjs.Tween.removeTweens(this);

		if (this.soundObj) {
			if (this.soundObj.src) {
				this.soundObj.stop();

				// just another catch to remove the sound, prob not needed
				if (createjs.Sound._preloadHash[this.soundObj.src]) {
					createjs.Sound.removeSound(this.soundObj.src);
				}
			}
			this.soundObj.removeAllEventListeners();
			createjs.Tween.removeTweens(this.soundObj);
		}

		this.soundObj = null;
		this.index = 0;
		this.isPlaying = false;

		let completeCallback = this.completeCallback;
		let completeScope = this.completeScope;
		let completeParams = this.completeParams;
		this.completeCallback = null;
		this.completeScope = null;
		this.completeParams = null;

		if (callCompleteCallbacks && completeCallback) {
			completeCallback.apply(completeScope, completeParams);
		}

		return this;
	}

	/**
	 * Fades the sequence out over time.
	 * See {@link VoiceOver|VoiceOver.stop} for reference on `callCompleteCallbacks`.
	 * @param {Number} [time=600] 
	 * @param {Boolean} [callCompleteCallbacks=false]
	 * @return {createjs.Tween} {@link https://createjs.com/docs/tweenjs/classes/Tween.html|CreateJS Tween}
	 */
	fadeOut(time = 600, callCompleteCallbacks = false) {
		this.isPlaying = false;
		createjs.Tween.removeTweens(this);
		return createjs.Tween.get(this.soundObj || {}, { override: true }).to({ volume: 0 }, time).call(this.stop, [callCompleteCallbacks], this);
	}

	/**
	 * Can be called manually, and is called internally when losing focus on the window.
	 * Resumes the current running sound.
	 * @return {VoiceOver}
	 */
	resume() {
		if (this.soundObj) {
			this.soundObj.paused = false;//setPaused(false);
		}
		return this;
	}

	/**
	 * Can be called manually, and is called internally when gaining focus on the window.
	 * Pauses the current running sound.
	 * @return {VoiceOver}
	 */
	pause() {
		if (this.soundObj) {
			this.soundObj.paused = true;//setPaused(true);
		}
		return this;
	}

	/**
	 * @return {boolean}
	 */
	isPaused() {
		return this.soundObj ? this.soundObj.paused : false;
	}
}

export default new VoiceOver();