# Playhouse
The aim of this framework is to be a quick and simple game framework to get going on your projects. It provides most common features used for game development: preloading, background music, input, saving data, timers, and some math helpers to name a few. 

It is built with the [CreateJS](http://www.createjs.com) suite in mind. The suite isn't included however, that is on your end. It was developed with CreateJS 1.x.x.

## Documentation
Run the command `npm run docs` for this module and it will create a folder `docs` which you can navigate to.

### License
[MIT License](https://opensource.org/licenses/MIT)

```
notes to developer when updating:
npm run build
npm version [major|minor|patch]
[commit and push to repo]
git push --tags origin es6
npm publish --access public
```