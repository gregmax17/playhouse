/**
 * The Loader uses {@link https://createjs.com/docs/preloadjs/modules/PreloadJS.html|PreloadJS} to load the assets needed.
 * 
 * Before using it, _you must_ set an `Array` of assets, which each item in the array being an object.
 * 
 * This class is already internally instantiated.
 * 
 * @example
 * // each object in the array MUST contain at least a "id", "src", and "tags" property
 * let assets = {
 *	{ "id": "config", "src": "media/data/config.json", "tags": ["pre"] },
 * 	{ "id": "sprites-ui", "src": "media/images/ui.json", "type": "spritesheet", "tags": ["title", "game"] },
 * 	{ "id": "sprites-title", "src": "media/images/title.json", "type": "spritesheet", "tags": ["title"] },
 * 	{ "id": "music", "src": "media/audio/music.ogg", "tags": ["game"] }
 * };
 * 
 * // set the assets to the Loader
 * Loader.setAssets(assets);
 * 
 * // load the assets that are tagged 'game'
 * Loader.load("game", () => {
 * 	alert("loaded assets with the tags 'game'!");
 * });
 * 
 * // can load multiple tags assets
 * Loader.load(["title", "game"], () => {
 * 	alert("loaded!");
 * });
 * @namespace Loader
 * @property {Number} status The current load status, useful for loaders.
 * @property {boolean} [useXHR=false] Whether to use XHR requests or not.
 * @property {String} basePath The base path to use when loading assets.
 * @property {Number} loadTimeout The time it takes to an item before timing out.
 */
class Loader {
	status = 0;
	useXHR = false;
	basePath = '';
	loadTimeout = 1000 * 60;
	loadedObjects = {};
	plugins = [];

	constructor() {
		this.addPlugin(createjs.Sound);
	}

	/**
	 * Will return the loaded asset via `id` if it exists, or will return `null`.
	 * @example
	 * Loader.get("sprites-ui"); // will return null
	 * 
	 * // load the assets with the tag 'game'
	 * Loader.load("game", () => {
	 * 	let ss = Loader.get("sprites-ui"); // will return a sprite sheet object
	 * 
	 * 	// then...
	 * 	createjs.Sprite(ss, "play-sprite");
	 * });
	 * @param {String} id 
	 * @return {*}
	 */
	get(id) {
		return this.loadedObjects[id] || null;
	}

	/**
	 * Will set a `value` via `id`, used mainly for internal use.
	 * You may also set delete a reference to a loaded asset by setting its value to `null`.
	 * @example
	 * Loader.set("config", null); // gone, will have to load it again
	 * @param {String} id 
	 * @param {*} value 
	 */
	set(id, value) {
		this.loadedObjects[id] = value;

		if (value === null) {
			delete this.loadedObjects[id];
		}
	}

	/**
	 * Add an external plugin which can be used.
	 * @param {Class} plugin 
	 */
	addPlugin(plugin) {
		this.plugins.push(plugin);
	}

	/**
	 * Used to load the assets by `tags`.
	 * @example
	 * Loader.load("title", (number, str) => {
	 * 	console.log(number, "and", str);
	 * }, [123, "some string value"]);
	 * @param {String|Array} tags Can be either a String or an Array of strings.
	 * @param {Function} [callback] Will be called once all assets are loaded by the tag name.
	 * @param {Array} [params] The paramets to into the `callback` once loaded.
	 * @param {Object} [scope] The scope to use on the `callback`.
	 */
	load(tags, callback, params, scope) {
		this.loadCallback = callback || (() => { });
		this.loadParams = params || null;
		this.loadScope = scope || null;

		let manifest = this.getAssetsByTags(tags);

		for (let i = 0; i < manifest.length; i++) {
			if (this.get(manifest[i].id)) {
				manifest.splice(i, 1);
				i--;
				continue;
			}
		}

		if (manifest.length) {
			this.loadManifest(manifest);
		}
		else {
			this.handleComplete();
		}
	}

	/**
	 * Called internally. 
	 * Creates an intance of {@link https://createjs.com/docs/preloadjs/classes/LoadQueue.html|PreloadJS's LoadQueue} class.
	 * @param {Array} manifest 
	 */
	loadManifest(manifest) {
		if (!this.loadQueue) {
			this.status = 0;
			this.loadQueue = new createjs.LoadQueue(this.useXHR, this.basePath);
			this.loadQueue.on('fileload', this.handleFileLoad, this);

			// let the browser max it self out, we want to load all we can to be quicker
			this.loadQueue.setMaxConnections(32);

			// apply the plugins
			this.plugins.forEach((plugin) => {
				this.loadQueue.installPlugin(plugin);
			});

			// one minute timeout for loading
			createjs.LoadItem.LOAD_TIMEOUT_DEFAULT = this.loadTimeout;
		}

		this.loadQueue.loadManifest(manifest);
	}

	/**
	 * Called internally. 
	 * Sets the `status` property based on `loaded objects / objects needed to be load`.
	 * @param {Object} event 
	 */
	handleFileLoad(event) {
		// update status
		this.status = this.loadQueue ? this.loadQueue.getItems(true).length / this.loadQueue.getItems().length : this.status;
		this.set(event.item.id, event.result);

		if (this.status >= 1) {
			this.handleComplete();
		}
	}

	/**
	 * Called internally.
	 * Will be called when all the assets have been loaded.
	 * Destroys the `CreateJS.LoadQueue` instance.
	 */
	handleComplete() {
		// complete
		this.status = 1;

		var loadCallback = this.loadCallback;
		var loadScope = this.loadScope;
		var loadParams = this.loadParams;
		this.loadCallback = null;
		this.loadScope = null;
		this.loadParams = null;

		var loadQueue = this.loadQueue;
		this.loadQueue = null;

		if (loadQueue) {
			loadQueue.unregisterLoader();
			loadQueue.removeAll();
			loadQueue.removeAllEventListeners();
		}

		if (loadCallback) {
			loadCallback.apply(loadScope, loadParams);
		}
	}

	/**
	 * The `Array` of assets to set for the `Loader`.
	 * @param {Array} assets 
	 */
	setAssets(assets) {
		this.assets = assets;
	}

	/**
	 * Returns the asset object defined in the assets array (which should have been via {@link Loader|setAssets})
	 * @param {String} id
	 * @return {Object}
	 */
	getAssetById(id) {
		for(let i = 0; i < this.assets.length; i++ ) {
			if ( this.assets[i].id === id) {
				return this.assets[i];
			}
		}
		return null;
	}

	/**
	 * Will return an `Array` of the assets that match the `tags`
	 * @param {String|Array} tags Can be either a String or an Array of strings.
	 * @return {Array}
	 */
	getAssetsByTags(tags) {
		if(!this.assets) {
			throw 'You need to let the `Loader` know about your assets! Call `Loader.setAssets([assets])` and pass the assets array!';
		}

		tags = typeof tags === 'string' ? [tags] : tags;
		let manifest = [];
		for (let i = 0; i < this.assets.length; i++) {
			for (let j = 0; j < tags.length; j++) {
				if ((this.assets[i].tags || []).indexOf(tags[j]) > -1) {
					manifest.push(this.assets[i]);
				}
			}
		}
		return manifest;
	}
}

export default new Loader();