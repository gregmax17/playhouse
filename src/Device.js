/**
 * Contains numerous properties with their respective values about the current users device.
 * The values are determined by reading the `navigator.userAgent` string.
 * @namespace Device
 */
let ua = navigator.userAgent;

let pixelRatio = window ? window.devicePixelRatio || 1 : 1;

// ios
let iPod = /ipod/i.test(ua);
let iPhone = /iphone/i.test(ua);
let iPad = /ipad/i.test(ua);
let iPad2 = iPad && pixelRatio === 1;
let iOS = iPod || iPhone || iPad;
let iOSVersion = iOS ? parseFloat(((ua.match(/os (\d+_\d+)?/i) || ['', ''])[1] + '').replace('_', '.')) : -1;

// android/silk
let kindle = /silk/i.test(ua) || /\ KF[A-Z]{4,}\ /.test(ua); // https://developer.amazon.com/docs/fire-tablets/ft-specs-custom.html#UserAgentStrings
let android = /android/i.test(ua);
let androidPhone = /(?=.*\bandroid\b)(?=.*\bmobile\b)/i.test(ua);
let androidTablet = (!androidPhone && android) || kindle;
android = androidPhone || androidTablet;
let androidVersion = ua.match(/android\s([0-9\.]*)/i);
androidVersion = androidVersion ? androidVersion[1] : -1;

// windows
let winPhone = /(iemobile|windows phone)/i.test(ua);
let winTablet = /(?=.*\bwindows\b)(?=.*\\btouch\b)/i.test(ua);

// browser
let chrome = /chrome/i.test(ua);
let safari = /safari/i.test(ua) && !chrome;
let ie = /msie 9/i.test(ua) || !!ua.match(/trident.*rv:/i); // lets not bother looking for version. if this is true, thats bad news...
let edge = /edge/i.test(ua);
let firefox = /firefox/i.test(ua);
let opera = /opera/i.test(ua) || /opr/i.test(ua);

// misc
let mobile = iOS || android || winPhone || winTablet || /mobile/i.test(ua);
let tablet = iPad || androidTablet || winTablet;
let cordova = /^file:\/{3}[^\/]/i.test(window ? window.location.href : '');
let crosswalk = /crosswalk/i.test(ua);
let cocoon = !!navigator.isCocoonJS;
let ejecta = /ejecta/i.test(ua);
let facebook = /fb/i.test(ua);
let wiiu = /nintendo wiiu/i.test(ua);

let touchDevice = ('ontouchstart' in window) || (window ? window.navigator.msMaxTouchPoints : false);

export default {
	/**
	 * @memberof Device
	 * @property {Number} pixelRatio The user's pixel ratio.
	 */
	pixelRatio: pixelRatio,

	/**
	 * @memberof Device
	 * @property {boolean} iPod
	 */
	iPod: iPod,

	/**
	 * @memberof Device
	 * @property {boolean} iPhone
	 */
	iPhone: iPhone,

	/**
	 * @memberof Device
	 * @property {boolean} iPad
	 */
	iPad: iPad,

	/**
	 * @memberof Device
	 * @property {boolean} iPad2
	 */
	iPad2: iPad2,

	/**
	 * @memberof Device
	 * @property {boolean} iOS
	 */
	iOS: iOS,

	/**
	 * @memberof Device
	 * @property {Number} iOSVersion
	 */
	iOSVersion: iOSVersion,

	/**
	 * @memberof Device
	 * @property {boolean} kindle
	 */
	kindle: kindle,

	/**
	 * @memberof Device
	 * @property {boolean} android
	 */
	android: android,

	/**
	 * @memberof Device
	 * @property {boolean} androidPhone
	 */
	androidPhone: androidPhone,

	/**
	 * @memberof Device
	 * @property {boolean} androidTablet
	 */
	androidTablet: androidTablet,

	/**
	 * @memberof Device
	 * @property {Number} androidVersion
	 */
	androidVersion: androidVersion,

	/**
	 * @memberof Device
	 * @property {boolean} winPhone
	 */
	winPhone: winPhone,

	/**
	 * @memberof Device
	 * @property {boolean} winTablet
	 */
	winTablet: winTablet,

	/**
	 * @memberof Device
	 * @property {boolean} chrome
	 */
	chrome: chrome,

	/**
	 * @memberof Device
	 * @property {boolean} safari
	 */
	safari: safari,

	/**
	 * @memberof Device
	 * @property {boolean} ie
	 */
	ie: ie,

	/**
	 * @memberof Device
	 * @property {boolean} edge
	 */
	edge: edge,

	/**
	 * @memberof Device
	 * @property {boolean} firefox
	 */
	firefox: firefox,

	/**
	 * @memberof Device
	 * @property {boolean} opera
	 */
	opera: opera,

	/**
	 * @memberof Device
	 * @property {boolean} mobile
	 */
	mobile: mobile,
	
	/**
	 * @memberof Device
	 * @property {boolean} tablet
	 */
	tablet: tablet,

	/**
	 * @memberof Device
	 * @property {boolean} cordova
	 */
	cordova: cordova,

	/**
	 * @memberof Device
	 * @property {boolean} crosswalk
	 */
	crosswalk: crosswalk,

	/**
	 * @memberof Device
	 * @property {boolean} cocoon
	 */
	cocoon: cocoon,

	/**
	 * @memberof Device
	 * @property {boolean} ejecta
	 */
	ejecta: ejecta,

	/**
	 * @memberof Device
	 * @property {boolean} facebook
	 */
	facebook: facebook,

	/**
	 * @memberof Device
	 * @property {boolean} wiiu
	 */
	wiiu: wiiu,

	/**
	 * @memberof Device
	 * @property {boolean} touchDevice
	 */
	touchDevice: touchDevice,

	/**
	 * Called internally.
	 * A fix for WebAudio in a Cordova environment.
	 * This will automatically get called in the {@link Play|Play.constructor} constructor if `Device.cordova` is `true`.
	 * @memberof Device
	 */
	fixWebAudio: () => {
		var WebAudioContext = window.AudioContext || window.webkitAudioContext;

		if (WebAudioContext) {
			try {
				var context = new WebAudioContext();
				var buffer = context.createBuffer(1, 1, 44100);
				var audioSource = context.createBufferSource();
				audioSource.buffer = buffer;
				audioSource.connect(context.destination);
				audioSource.start(0);
				audioSource.disconnect();

				if (context && context.close) {
					context.close();
				}
			}
			catch (e) {
				// something changed in the iOS webview.
			}
		}
	}
};