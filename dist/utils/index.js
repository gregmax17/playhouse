"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Utils = void 0;

var _Array2 = require("./Array.js");

var _Math2 = require("./Math.js");

var _Number2 = require("./Number.js");

var _Misc = require("./Misc.js");

/**
 * @namespace Utils
 */
var Utils = {
  Array: _Array2._Array,
  Math: _Math2._Math,
  Number: _Number2._Number,
  getUrlVar: _Misc.getUrlVar
};
exports.Utils = Utils;