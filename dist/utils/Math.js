"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._Math = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @namespace _Math
 * @name Math
 */
var _Math =
/*#__PURE__*/
function () {
  function _Math() {
    _classCallCheck(this, _Math);
  }

  _createClass(_Math, null, [{
    key: "random",

    /**
    * Returns a `Number` between the `min[inclusive]` and `max[inclusive]`. 
    * If both values passed are whole numbers, a whole number will be returned, other a decimal value will be returned. 
    * If both parameters are omitted it will just return a `Math.random()` value.
    * @example
    * Math.random(-10, 10); // returns a random int value between -10 and 10
    * Math.random(0.1, 0.9); // will return a random decimal value between 0.1 and 0.9
    * Math.random(); // alias for Math.random()
    * @memberof Math
    * @name random
    * @param {Number} [min]
    * @param {Number} [max]
    * @return {Number}
    */
    value: function random(min, max) {
      var n = Math.random(); // default

      if (!min && !max) {
        return n;
      } // return an int
      else if (min % 1 === 0 && max % 1 === 0) {
          return Math.floor(n * (max - min + 1)) + min;
        } // return decimal
        else {
            return n * (max - min) + min;
          }
    }
    /**
    * Linearly interpolates between `current` and `target` by `percent`.
    * If `clamped` is set to `false` the `percent` value can exceed values betwen 0 and 1. (Defeault is `true`)
    * @example
    * Math.lerp(0, 100, 0.4); // will return 40
    * Math.lerp(-100, 100, 0.5); // will return 0
    * Math.lerp(0, 10, 1.2, false); // will return 12
    * @memberof Math
    * @name lerp
    * @param {Number} current 
    * @param {Number} target 
    * @param {Number} percent 
    * @param {bool} [clamped=true]
    * @param {Number}
    */

  }, {
    key: "lerp",
    value: function lerp(current, target, percent) {
      var clamped = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : true;

      if (clamped) {
        percent = _Math.clamp(percent, 0, 1);
      }

      return current + percent * (target - current);
    }
    /**
     * Can be used in a combination of ways. 
     * @example
     * // with four/five parameters
     * Math.distance(0, 0, 10, 10); // will return 400
     * Math.distance(0, 0, 10, 10, true); // setting the 5th parameter will return the `Math.sqrt` distance value, 14.142...
     * 
     * // with two/three paramteres
     * let obj1 = { x : 5, y : 5 };
     * let obj2 = { x : 20, y : 20 };
     * Math.distance(obj1, obj2); // will return 450
     * Math.distance(obj1, obj2, true); // the 3rd parameter will now determine if to use `Math.sqrt`, and in this case the value will be 21.213...
     * @memberof Math
     * @name distance
     * @param {*} x1 can be a `Number` or `Object`
     * @param {*} y1 can be a `Number` or `Object`
     * @param {*} [x2] can be a `Number` or `boolean` value
     * @param {Number} [y2] must be a `Number`
     * @param {boolean} [useSqrt=false] must be a `boolean` value
     * @return {Number}
     */

  }, {
    key: "distance",
    value: function distance(x1, y1, x2, y2) {
      var useSqrt = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
      var dx, dy;

      if (arguments.length > 3) {
        dx = x2 - x1;
        dy = y2 - y1;
      } else {
        dx = y1.x - x1.x;
        dy = y1.y - x1.y;
        useSqrt = x2;
      }

      return useSqrt ? Math.sqrt(dx * dx + dy * dy) : dx * dx + dy * dy;
    }
    /**
     * Can be used in a combination of ways. 
     * @example
     * // with four/five parameters
     * Math.angle(0, 0, 10, 10); // will return the value 0.785
     * Math.angle(0, 0, 10, 10, true); // will return the value 45
     * 
     * // with two/three paramteres
     * let obj1 = { x : 0, y : 5 };
     * let obj2 = { x : 5, y : 15 };
     * Math.angle(obj1, obj2, true); // will return the value in degrees with 63.434...
     * @memberof Math
     * @name angle
     * @param {*} x1 can be a `Number` or `Object`
     * @param {*} y1 can be a `Number` or `Object`
     * @param {*} [x2] can be a `Number` or `boolean` value
     * @param {Number} [y2] must be a `Number`
     * @param {boolean} [useDegrees=false] must be a `boolean` value
     * @return {Number}
     */

  }, {
    key: "angle",
    value: function angle(x1, y1, x2, y2) {
      var useDegrees = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
      var r;

      if (arguments.length > 3) {
        r = Math.atan2(y2 - y1, x2 - x1);
      } else {
        r = Math.atan2(y1.y - x1.y, y1.x - x1.x);
        useDegrees = x2;
      }

      return useDegrees ? _Math.toDeg(r) : r;
    }
    /**
     * Clamps the `number` value between the `min` and `max` value.
     * @example
     * Math.clamp(-50, 0, 100); // will return the value of 0
     * Math.clamp(100, 0, 500); // will return the value of 100
     * Math.clamp(12, 0, 10); // will return 10
     * @memberof Math
     * @name clamp
     * @param {Number} number 
     * @param {Number} min 
     * @param {Number} max 
     * @return {Number}
     */

  }, {
    key: "clamp",
    value: function clamp(number, min, max) {
      return number > max ? max : number < min ? min : number;
    }
    /**
     * Returns the radians value from the degrees value.
     * @memberof Math
     * @name toRad
     * @param {Number} degrees 
     * @return {Number}
     */

  }, {
    key: "toRad",
    value: function toRad(degrees) {
      return degrees / 180 * Math.PI;
    }
    /**
     * Returns the degrees value from the radians value.
     * @memberof Math
     * @name toDeg
     * @param {Number} radians 
     * @return {Number}
     */

  }, {
    key: "toDeg",
    value: function toDeg(radians) {
      return radians * 180 / Math.PI;
    }
  }]);

  return _Math;
}();

exports._Math = _Math;